import java.util.ArrayList;

public class Trips {
    private final ArrayList<Trip> trips = new ArrayList<>();

    public void addTrip(Trip trip) {
        trips.add(trip);}

    public void write(){
        int count = 0;
        for (var t : trips)
        {
            System.out.printf("%2d.", ++count);
            t.write();
        }
    }

}
