
import java.time.LocalTime;

public class ABCDEF
{
   public static void main( String[] args )
   {
      var data = new Data();
      data.writeAllRoutes();
      System.out.println();
      System.out.format( "=".repeat(70));
      System.out.println();

      data.writeRoutes("B", "C", LocalTime.of(16,0));

      System.out.println();
      System.out.format( "=".repeat(70));
      System.out.println();

      var trips = data.getTrips("B","C", LocalTime.of(16,0));
      trips.write(); // plan je reis

   }
}
