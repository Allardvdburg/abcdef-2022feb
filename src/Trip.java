import java.time.LocalTime;

public class Trip {

   private final Route route;
   private final LocalTime departure;
   private final Location locationA;
   private final Location locationB;

   public Trip(Route route, LocalTime departure, Location locationA, Location locationB)
    {
        this.route = route;
        this.departure = departure;
        this.locationA = route.stopOvers.get(0);
        this.locationB = route.stopOvers.get(route.stopOvers.size()-1);
    }

    public LocalTime getDeparture() {return departure;}

    public void write() {
       route.write();
    }
}
