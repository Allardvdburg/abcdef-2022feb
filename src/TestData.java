public class TestData extends Data {

        public void writeRoutesABCD() {
            int count = 0;
            for (var e : routeMap.entrySet()) {
                var key = e.getKey();
                var route = e.getValue();
                if (key.contains("A-B-C-D")) {
                    System.out.printf("%3d. ", ++count);
                    route.write();
                }
            }
        }

        public void writeRoutesBD() {
            int count = 0;
            for (var e : routeMap.entrySet()) {
                var key = e.getKey();
                var route = e.getValue();

                var posB = key.indexOf("B");
                var posD = key.indexOf("D");

                if (posD > posB) {
                    System.out.printf("%3d. ", ++count);
                    route.write();
                }
            }
        }
    }
