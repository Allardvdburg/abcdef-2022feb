import java.time.LocalTime;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class Data {
   protected final HashMap<String, Location> locationMap = new HashMap<>();
   protected final SortedMap<String, Route> routeMap = new TreeMap<>();

   public Data() {
      /// === Locations A ... F ========

      var location = new Location("A");
      locationMap.put(location.getName(), location);

      location = new Location("B");
      locationMap.put(location.getName(), location);

      location = new Location("C");
      locationMap.put(location.getName(), location);

      location = new Location("D");
      locationMap.put(location.getName(), location);

      location = new Location("E");
      locationMap.put(location.getName(), location);

      location = new Location("F");
      locationMap.put(location.getName(), location);

      ////////////////////////////////////////////////////////////

      /// === Routes A-B-C-D ========
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("A"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 32));
         route.addEndPoint(locationMap.get("D"), LocalTime.of(hour, 47));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes D-C-B-A
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("D"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 31), LocalTime.of(hour, 32));
         route.addEndPoint(locationMap.get("A"), LocalTime.of(hour, 47));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes E-B-C-F ========
      for (int hour = 8; hour <= 17; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("E"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 1).plusHours(1), LocalTime.of(hour, 2));
         route.addEndPoint(locationMap.get("F"), LocalTime.of(hour, 17).plusHours(1));
         routeMap.put(route.getKey(), route);
      }


      /// === Routes F-C-B-E ========
      for (int hour = 8; hour <= 17; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("F"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 1).plusHours(1), LocalTime.of(hour, 2));
         route.addEndPoint(locationMap.get("E"), LocalTime.of(hour, 17).plusHours(1));
         routeMap.put(route.getKey(), route);
      }

      // === Route B-C ===
      for (int hour = 12; hour <= 12; hour += 24) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("B"), departure);
         route.addEndPoint(locationMap.get("C"), LocalTime.of(hour, 15));
         routeMap.put(route.getKey(), route);
      }

      // === Route C-B ===
      for (int hour = 12; hour <= 12; hour += 24) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("C"), departure);
         route.addEndPoint(locationMap.get("B"), LocalTime.of(hour, 15));
         routeMap.put(route.getKey(), route);
      }
   }

   public void writeAllRoutes() {
      int count = 0;
      for (var route : routeMap.values()) {
         System.out.printf("%3d. ", ++count);
         route.write();
      }
   }

   public void writeRoutes(String LocationA, String LocationB, LocalTime t) {
      var count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var pos1 = key.indexOf(LocationA);
         if (pos1 >= 0) {
            var pos2 = key.indexOf(LocationB);
            if (pos2 > pos1) {
               var route = e.getValue();
               var halte = route.getStopOver(LocationA);
               assert (halte != null);
               if (halte.getDeparture().isAfter(t) || halte.getDeparture().equals(t)) {
                  System.out.format("%3d.", ++count);
                  route.write();
               }
            }
         }
      }
   }

   public Trips getTrips(String LocationA, String LocationB, LocalTime t) {
      Trips trips = new Trips();
      var count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var pos1 = key.indexOf(LocationA);
         if (pos1 >= 0) {
            var pos2 = key.indexOf(LocationB);
            if (pos2 > pos1) {
               var route = e.getValue();
               var halte = route.getStopOver(LocationA);
               assert (halte != null);
               if (halte.getDeparture().isAfter(t) || halte.getDeparture().equals(t)) {
                  trips.addTrip(new Trip(route, halte.getDeparture(), route.stopOvers.get(0), route.stopOvers.get(route.stopOvers.size() - 1)));
               }

            }
         }
      }
      return trips;
   }
}